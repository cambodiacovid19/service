<?php

use Illuminate\Database\Seeder;

class CaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('case')->insert(
            [
                ['name' => 'Admin'],
                ['name' => 'User'],
            ]
        );

        // ============================================================  Admin
        $userId = DB::table('user')->insertGetId([ 
            'type_id'=>1, 
            'email'=>'admin.covid@cambodia.com',                   
            'phone' => '070454047', 
            'password' => bcrypt('123456'), 
            'is_active'=>1, 
            'is_email_verified'=>1, 
            'is_phone_verified'=>1,
            'name' => 'Cambodia Covid-19'
        ]);
        
    }
}
