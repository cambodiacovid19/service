<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->integer('nationality_id')->unsigned()->index()->nullable();
            $table->foreign('nationality_id')->references('id')->on('nationality')->onDelete('cascade');

            $table->integer('province_id')->unsigned()->index()->nullable();
            $table->foreign('province_id')->references('id')->on('province')->onDelete('cascade');

            $table->integer('district_id')->unsigned()->index()->nullable();
            $table->foreign('district_id')->references('id')->on('district')->onDelete('cascade');

            $table->integer('commune_id')->unsigned()->index()->nullable();
            $table->foreign('commune_id')->references('id')->on('commune')->onDelete('cascade');

            $table->integer('linked_case_id')->unsigned()->index()->nullable();
            $table->foreign('linked_case_id')->references('id')->on('case')->onDelete('cascade');

            $table->integer('linked_type_id')->unsigned()->index()->nullable();
            $table->foreign('linked_type_id')->references('id')->on('linked_type')->onDelete('cascade');

            $table->integer('expose_id')->unsigned()->index()->nullable();
            $table->foreign('expose_id')->references('id')->on('expose')->onDelete('cascade');

            $table->integer('sex_id')->unsigned()->index()->nullable();
            $table->foreign('sex_id')->references('id')->on('sex')->onDelete('cascade');

            $table->integer('hospital_id')->unsigned()->index()->nullable();
            $table->foreign('hospital_id')->references('id')->on('hospital')->onDelete('cascade');

            $table->integer('event_id')->unsigned()->index()->nullable();
            $table->foreign('event_id')->references('id')->on('event')->onDelete('cascade');

            $table->string('kh_name',500)->nullable();
            $table->string('en_name',500)->nullable();
            $table->string('number', 50)->unique()->nullable();
            $table->string('age', 50)->nullable();
            $table->dateTime('confirmed_date')->nullable();
            $table->dateTime('discharged_date')->nullable();
            $table->dateTime('death_date')->nullable();

            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('updater_id')->unsigned()->nullable();
            $table->integer('deleter_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case');
    }
}
