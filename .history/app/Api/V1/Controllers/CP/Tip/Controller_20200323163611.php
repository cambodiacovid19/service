<?php

namespace App\Api\V1\Controllers\CP\Tip;

use Illuminate\Http\Request;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Tip\Main;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

// Import Resource
use App\Api\V1\Resources\Tip\TipResource;
use App\Api\V1\Resources\Tip\TipCollection;

class Controller extends ApiController
{
    use Helpers;
    function listing(Request $req) {
        $data = Main::select(
                'id', 
                'en_title', 
                'kh_title',
                'en_description', 
                'kh_description',
                'image', 
                'created_at'
                )
        ->get();
        $data = new TipCollection($data);
        return response()->json($data, 200);
    }

    function view($id = 0){
        $data   = Main::select(
            'id', 
            'en_title', 
            'kh_title',
            'en_description', 
            'kh_description',
            'image',
            'created_at'
            )
        ->find($id); 

        if($data){
            $data = new TipResource($data);
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function create(Request $req)
    {
        $this->validate($req, [
            'en_title'              => 'required',
           
           
        ], [
            'en_title.required'         => 'Plese enter tilte.',

        ]);

        $data                       = new Main();
        $data->en_title                = $req->en_title;
        $data->en_description          = $req->en_description;

         //Need to create folder before storing images
    
         $image = FileUpload::uploadImage($req, 'image', ['uploads', '/news','/' .uniqid(), '/image'], [['xs', 200, 200]]);
         if ($image != "") {
             $data->image = $image;
         }

        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'en_title'              => 'required|max:60',
            
        ], [
            'en_title.required'         => 'Plese enter title .',
            
        ]);

        $data   = Main::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->en_title              = $req->en_title;
            $data->en_description        = $req->en_description;

             //Need to create folder before storing images
            $image = FileUpload::uploadImage($req, 'image', ['uploads', '/news','/' .uniqid(), '/image'], [['xs', 200, 200]]);
            if ($image != "") {
                $data->image = $image;
            }
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function delete($id = 0)
    {
        $data   = Main::find($id); 
        if($data){
            // Start to update
            $data->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

}
