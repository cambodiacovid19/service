<?php

namespace App\Api\V1\Controllers\CP\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\User\Main;
use App\Model\Member\Main as Member;
use Dingo\Api\Routing\Helpers;
use JWTAuth;



class Controller extends ApiController
{
    use Helpers;
       

    function listing($id = 0){
     
       if($id!=0){
        $data = Main::select('*')->findOrFail($id);
        if($data){
            return response()->json(['data'=>$data], 200);
        }else{
            return response()->json(['status_code'=>404], 404);
        }
    }
        $data    = Main::select('id', 'name', 'phone', 'avatar', 'email');
            $key = isset($_GET['key'])?$_GET['key']:"";
            if($key){
                $data = $data->where('name', 'like', $key)->orWhere('phone', 'like', $key)->orWhere('email', 'like', $key);
            }

            $from=isset($_GET['from'])?$_GET['from']:"";
            $to=isset($_GET['to'])?$_GET['to']:"";
            if(isValidDate($from)){
                if(isValidDate($to)){
                    $from .=" 00:00:00";
                    $to .=" 23:59:59";
                    $data = $data->whereBetween('created_at', [$from, $to]);
                }
            }
            $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
            $data= $data->orderBy('id', 'desc')->paginate($limit); 
            return $data;
    }

    function view($id = 0){
        $data   = Main::select('id', 'name', 'phone', 'avatar', 'email')->find($id);

        if($data){
            return response()->json(['data' => $data], 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'Record not found.'
            ], 404);
        }
        
    }
    
    function create(Request $request){
        $admin = JWTAuth::parseToken()->authenticate();

        $this->validate($request, [
            'name' => 'required|max:150',
            'phone' =>  [
                            'required', 
                            'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                            Rule::unique('user', 'phone')
                        ],
            'email'=>   [
                            'sometimes', 
                            'required', 
                            'email', 
                            'max:100', 
                            Rule::unique('user', 'email')
                        ],
            ],[

            'name.required'       =>   'Please enter your name.',
            'name.max'            =>   'Name has been digit  to 60.',

            'phone.required'      =>   'Please enter your phone number.',
            'phone.regex'         =>   'Phone number is required.',
            'phone.unique'        =>   'Phone number already exists.',

            'email.required'      =>   'Please enter your email.',
            'email.email'         =>   'Email is required.',
            'email.max'           =>  'email has been digit  to 100.',
            'email.unique'        =>   'Email already exists.',

            ]);
   
        $user = new Main();
        $user->type_id =1;
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->is_active = 1;
        $user->password = bcrypt($request->password);
        $user->avatar = '{}';
        $user->creator_id = $admin->id;
        $user->updater_id = $admin->id;
        $user->save();

        // $admin = new Main();
        // $admin->user_id     = $user->id;
        // $admin->creator_id  = $admin->id;
        // $admin->updater_id  = $admin->id;
        // $admin->save();
          //Need to create folder before storing images
        
        $last = Main::select('id')->orderBy('id', 'DESC')->first();
        $id = 0;
        if($last){
            $id = $last->id+1;
        }

        $avatar = FileUpload::image($request, 'avatar', ['uploads', '/admin', '/'.$id], [['xs', 200, 200]]);
        if($avatar != ""){
            $user->avatar = $avatar;
        }
        $avatar = FileUpload::resize($request, 'avatar', ['uploads', '/admin', '/'.$id], [['xs', 200, 200]]);
        if($avatar != ""){
            $user->avatar = $avatar;
        }
        $user->save();   
        return response()->json([
            'status'    => 'success',
            'message'   => 'Succefully created', 
            'admin'     => $admin,
            'user'      => $user,
        ], 200);
    }
    function update(Request $request, $id=0){

     
        $admin     = JWTAuth::parseToken()->authenticate();

        $admin    = Main::select('*')
                    ->with([
                        'user:id,name,phone,email,is_active,avatar'
                    ])
                    ->find($id);
      // return $id;
        if($admin){

            $user = $admin->user; 

            $this->validate($request,[

                'name' => 'required|max:150',
                'phone' =>  [
                                'required', 
                                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                                Rule::unique('user', 'phone')->ignore($user->id)
                            ],
                'email'=>   [
                                'sometimes', 
                                'required', 
                                'email', 
                                'max:100', 
                                Rule::unique('user', 'email')->ignore($user->id)
                            ],
                        ],[
                'name.required'       =>   'Please enter your name.',
                'name.max'            =>   'Name has been digit  to 60.',

                'phone.required'      =>   'Please enter your phone number.',
                'phone.regex'         =>   'Phone number is required.',
                'phone.unique'        =>   'Phone number already exists.',

                'email.required'      =>   'Please enter your email.',
                'email.email'         =>   'Email is required.',
                'email.unique'        =>  'Email already exists.',
                'email.max'           =>  'Email has been digit  to 100.',

              
            ]);

            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->updater_id = $admin->id;
            $user->save();
            $avatar = FileUpload::image($request, 'avatar', ['uploads', '/admin', '/'.$id], [['xs', 200, 200]]);
                if($avatar != ""){
                    $user->avatar = $avatar;
                }
            $avatar = FileUpload::resize($request, 'avatar', ['uploads', '/admin', '/'.$id], [['xs', 200, 200]]);
                if($avatar != ""){
                    $user->avatar = $avatar;
                }
                $user->save();
        
            return response()->json([
                'status' => 'success',
                'message' => 'Data has been updated.', 
                'admin' => $admin,
                'user'  => $user
            ], 200);

        }else{
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid admin record.', 
            
            ], 404);
        }
    }

    function delete($id=0){
        $data = Main::find($id);
        if(!$data){
            return response()->json([
                'message' => 'Data not found', 
            ], 404);
        }
        $data->delete();

        $User = User::find($data->user_id)->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Data has been deleted. ',
        ], 200);
    }

    /** Function To change Password */
    function changePassword(Request $request, $id=0){
        $admin     = JWTAuth::parseToken()->authenticate();
        $admin    = Main::select('*')
                    ->with([
                        'user:id,name,phone,email,is_active,avatar'
                    ])
                    ->find($id);
        if($admin){
            $user = $admin->user; 
            $this->validate($request,[
                'password' => 'required|min:6',
            ]);

            $user->password = bcrypt($request->password);
            $user->updater_id = $admin->id;
            $user->save();
        
            return response()->json([
                'status' => 'success',
                'message' => 'Data has been updated.', 
                'admin' => $admin
            ], 200);

        }else{
            return response()->json([
                'status' => 'fail',
                'message' => 'Invalid admin record.', 
            ], 404);
        }
    }

}
