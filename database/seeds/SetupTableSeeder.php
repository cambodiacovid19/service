<?php

use Illuminate\Database\Seeder;

class SetupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('country')->insert(
            [
                [ 'kh_name' => 'កម្ពុជា ', 'en_name' => 'Cambodia ',       'flag' => '{"xs":"public/country/cambodia.png"}'],
                [ 'kh_name' => 'ថៃ ', 'en_name' => 'Thailand ',      'flag' => '{"xs":"public/country/thailand.png"}'],
              
            ]
        );
        DB::table('province')->insert(
            [
                [
                    'country_id' => 1, 
                    'kh_name' => 'ភ្នំពេញ',
                    'en_name' => 'Phnom Penh',
                
                ],
                ['country_id' => 1, 
                'kh_name' => 'បន្ទាយមានជ័យ',
                'en_name' => 'Bantey Meanchey',
                ],
                ['country_id' => 1, 
                'kh_name' => 'កំពង់ស្ពឺ',
                'en_name' => 'Kampong Speu',
                ],  
                ['country_id' => 1, 
                'kh_name' => 'បាត់ដំបង',
                'en_name' => 'Battambang',
                ],              
                ['country_id' => 1, 
                'kh_name' => 'កំពង់ចាម',
                'en_name' => 'Kampong Cham',
                ],
                ['country_id' => 1, 
                'kh_name' => 'កំពង់ឆ្នាំង',
                'en_name' => 'Kampong Chhnang',
                ],
                ['country_id' => 1, 
                'kh_name' => 'កំពង់ធំ',
                'en_name' => 'Kampong Thom',
                ],
                ['country_id' => 1, 
                'kh_name' => 'កំពត',
                'en_name' => 'Kampot',
                ],
                ['country_id' => 1, 
                'kh_name' => 'កណ្តាល',
                'en_name' => 'Kandal',
                 ],
                ['country_id' => 1,
                 'kh_name' => 'កោះកុង',
                 'en_name' => 'Koh Kong',
                ],
                ['country_id' => 1, 
               
                'kh_name' => 'ក្រចេះ',
                'en_name' => 'Kratie',
                ],
                ['country_id' => 1, 
                'kh_name' => 'មណ្តលគីរី',
                'en_name' => 'Mondulkiri',
                ],
                
                ['country_id' => 1,
                'kh_name' => 'ព្រះវិហា',
                'en_name' => 'Preah Vihear',
                ],
                ['country_id' => 1, 
                'kh_name' => 'ព្រៃវែង',
                'en_name' => 'Prey Veng',
                ],
                ['country_id' => 1,
                    'kh_name' => 'ពោសាធន៏',
                    'en_name' => 'Pursat',
                ],
                ['country_id' => 1, 
                'kh_name' => 'រតនៈគីរី',
                'en_name' => 'Ratanakiri',
                ],
                ['country_id' => 1,
                 'kh_name' => 'សៀមរាម',
                 'en_name' => 'Siem Reap',
                ],
                ['country_id' => 1, 
                'kh_name' => 'ព្រះសីហានុ',
                 'en_name' => 'Preah Sihanouk',
                ],
                ['country_id' => 1,
                 'kh_name' => 'ស្ទឹងត្រែង',
                 'en_name' => 'Stung Treng',
                ],
                ['country_id' => 1, 
                'kh_name' => 'ស្វាយរៀង',
                'en_name' => 'Svay Rieng',
                ],
                ['country_id' => 1, 
                'kh_name' => 'តាកែវ',
                'en_name' => 'Takeo',
                
                ],
                ['country_id' => 1, 
                'kh_name' => 'ឧត្តរមានជ័យ',
                'en_name' => 'Otdar Meanchey',
                ],
                ['country_id' => 1, 
                'kh_name' => 'កែប',
                'en_name' => 'Kep',
                ],
                ['country_id' => 1, 
                'kh_name' => 'ប៉ៃលិន',
                'en_name' => 'Pailin',
                ],
                ['country_id' => 1, 
                'kh_name' => 'ត្បូងឃ្មំុ',
                'en_name' => 'Tbong Khmum',
                ],
            ]
        );

        DB::table('nationality')->insert(
            [
                [ 'kh_name' => 'ខ្មែរ', 'en_name' => 'Khmer', 'color' => '#ff5733' ],
                // [ 'kh_name' => 'ខ្មែរឥស្លាម', 'en_name' => 'Khmer Muslims', 'color' => '#A04000' ],
                [ 'kh_name' => 'ចិន', 'en_name' => 'Chinese', 'color' => '#884EA0' ],
                [ 'kh_name' => 'ជប៉ុន', 'en_name' => 'Japan', 'color' => '#616A6B' ],
                [ 'kh_name' => 'បារាំង', 'en_name' => 'France', 'color' => '#283747' ],
                [ 'kh_name' => 'អង់គ្លេស', 'en_name' => 'English', 'color' => '#1a5276' ],
            ]
        );
        DB::table('sex')->insert(
            [
                [ 'kh_name' => 'ប្រុស ', 'en_name' => 'Male ' ],
                [ 'kh_name' => 'ស្រី ', 'en_name' => 'Female '],
              
            ]
        );
        DB::table('linked_type')->insert(
            [
                [ 'kh_name' => 'សមាជិកគ្រួសារ', 'en_name' => 'Familiy Member ' ],
                [ 'kh_name' => 'អ្នករួមកាងារ', 'en_name' => 'Co-worker '],
              
            ]
        );
        DB::table('expose')->insert(
            [
                [ 'kh_name' => 'ក្នុងស្រុក', 'en_name' => 'Local' ],
                [ 'kh_name' => 'ក្រៅស្រុក', 'en_name' => 'Imported'],
              
            ]
        );

        DB::table('hospital')->insert(
            [
                [ 'kh_name' => 'មន្ទីពេទ្យរុស្សី', 'en_name' => 'Khmer Sovit Hospital' ]
            ]
        );
        DB::table('event')->insert(
            [
                [ 'kh_name' => 'ទំនាក់ទំនងជាមួយជនជាតិជប៉ុន', 'en_name' => 'Relations with the Japanese' ],
                [ 'kh_name'=> 'ពិធីសាសនានៅម៉ាឡេស៊ី', 'en_name' => 'Religion in Malaysia'],
                [ 'kh_name'=> 'សមាជិកគ្រួសារ', 'en_name' => 'Familly'],
                [ 'kh_name'=> 'ដំណើក្រៅប្រទេស', 'en_name' => 'Go Abroad'],
                [ 'kh_name'=> 'ផ្សេងៗ', 'en_name' => 'Other']
            ]
        );
    }
}
