<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->string('kh_name', 250)->nullable();
            $table->string('en_name', 250)->nullable();
            $table->string('phone', 250)->nullable();
            $table->string('kh_location', 250)->nullable();
            $table->string('en_location', 250)->nullable();
            $table->string('kh_address', 250)->nullable();
            $table->string('en_address', 250)->nullable();
            $table->string('lat', 250)->nullable();
            $table->string('lng', 250)->nullable();
        

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital');
    }
}
