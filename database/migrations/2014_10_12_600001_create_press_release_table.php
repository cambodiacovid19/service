<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePressReleaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('press_release', function (Blueprint $table) {
            $table->increments('id', 11);

            $table->string('kh_name', 250)->nullable();
            $table->string('en_name', 250)->nullable();
            $table->string('kh_description', 500)->nullable();
            $table->string('en_description', 500)->nullable();
            $table->string('kh_data',500)->nullable();
            $table->string('en_data',500)->nullable();
            
            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('updater_id')->unsigned()->nullable();
            $table->integer('deleter_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('press_release');
    }
}
