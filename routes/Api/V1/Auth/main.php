<?php


	
$api->group(['namespace' => 'App\Api\V1\Controllers\Auth'], function($api) {
     
        $api->post('/login', 			        ['uses' => 'LoginController@login']);  
        $api->post('/get-reset-password-code', 		['uses' => 'ForgotPasswordController@getResetPasswordCode']);   
        $api->post('/verify-reset-password-code', 	['uses' => 'ForgotPasswordController@verifyResetPasswordCode']);
        $api->post('/change-password', 	                ['uses' => 'ForgotPasswordController@changePassword']);   
        $api->get('/register/provinces', 		['uses' => 'RegisterController@provinces']);

        $api->post('/register', 			['uses' => 'RegisterController@register']);  
        $api->get('/register/get-account', 	        ['uses' => 'RegisterController@getAccount']);
        $api->post('/register/verify-account', 	        ['uses' => 'RegisterController@verifyAccount']);
        $api->post('/register/request-verify-code',     ['uses' => 'RegisterController@requestverifyCode']);
      
});