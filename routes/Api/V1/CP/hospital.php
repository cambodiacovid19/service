<?php
    $controller = 'Hospital\Controller@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);



