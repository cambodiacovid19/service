<?php
    $controller = 'Cases\Controller@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/setup', 		            ['uses'     => $controller.'setupData']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);

    $api->post('/verify-linkedcase', 		['uses'     => $controller.'verifyLinkedCases']);




