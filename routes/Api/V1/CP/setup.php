<?php
// National
$api->group(['prefix' => 'nationals'], function ($api) {
    $controller = 'Setup\NationalController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});

// Country
$api->group(['prefix' => 'countries'], function ($api) {
    $controller = 'Setup\CountryController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});

// Province
$api->group(['prefix' => 'provinces'], function ($api) {
    $controller = 'Setup\ProvinceController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});
// District
$api->group(['prefix' => 'districts'], function ($api) {
    $controller = 'Setup\DistrictController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});
// Commune
$api->group(['prefix' => 'communes'], function ($api) {
    $controller = 'Setup\CommuneController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});
// Sexs
$api->group(['prefix' => 'sexs'], function ($api) {
    $controller = 'Setup\SexController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});

// Link Typed
$api->group(['prefix' => 'link-types'], function ($api) {
    $controller = 'Setup\LinkTypeController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});

// Exposes
$api->group(['prefix' => 'exposes'], function ($api) {
    $controller = 'Setup\ExposeController@';
    $api->get('/', 				            ['uses'     => $controller.'listing']);
    $api->get('/{id}', 			            ['uses'     => $controller.'view']);
    $api->put('/{id}', 			            ['uses'     => $controller.'update']); 
    $api->post('/', 			            ['uses'     => $controller.'create']);
    $api->delete('/{id}', 		            ['uses'     => $controller.'delete']);
});
