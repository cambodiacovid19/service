<?php

	
$api->group(['namespace' => 'App\Api\V1\Controllers\CP'], function($api) {

		// ==================>> My Profile
		$api->group(['prefix' => 'my-profile'], function ($api) {
			require(__DIR__.'/my-profile.php');
		});

		// ==================>> Admin
		$api->group(['prefix' => 'admins'], function ($api) {
			require(__DIR__.'/admin.php');
		});

		// ==================>> Setup
		$api->group(['prefix' => 'setup'], function ($api) {
			require(__DIR__.'/setup.php');
		});

		// ==================>> Hospital
		$api->group(['prefix' => 'hospitals'], function ($api) {
			require(__DIR__.'/hospital.php');
		});
		// ==================>> New
		$api->group(['prefix' => 'news'], function ($api) {
			require(__DIR__.'/new.php');
		});

		// ==================>> Tip
		$api->group(['prefix' => 'tip'], function ($api) {
			require(__DIR__.'/tip.php');
		});

		// ==================>> Events
		$api->group(['prefix' => 'events'], function ($api) {
			require(__DIR__.'/event.php');
		});

		// ==================>> Press Release
		$api->group(['prefix' => 'press-releases'], function ($api) {
			require(__DIR__.'/press-release.php');
		});

		// ==================>> Case
		$api->group(['prefix' => 'cases'], function ($api) {
			require(__DIR__.'/case.php');
		});

		// ==================>> Case
		$api->group(['prefix' => 'visited-countries'], function ($api) {
			require(__DIR__.'/visited-country.php');
		});
	});