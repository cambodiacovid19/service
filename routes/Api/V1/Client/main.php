<?php

	
$api->group(['namespace' => 'App\Api\V1\Controllers\Client'], function($api) {

		// ==================>> My Profile
		$api->group(['prefix' => 'dashboard'], function ($api) {
			require(__DIR__.'/dashboard.php');
		});

		$api->group(['prefix' => 'world-datas'], function ($api) {
			require(__DIR__.'/world-data.php');
		});
	
	});