<?php

$controller = 'Dashboard\Controller@';
$api->get('/get-case-amount', 				['uses' => $controller.'getCaseAmount']);
$api->get('/get-dialy-new-case', 				['uses' => $controller.'dailyNewCase']);
$api->get('/get-new-discharge-case', 				['uses' => $controller.'dischargeCase']);
$api->get('/get-provinces', 				['uses' => $controller.'getProvices']);
$api->get('/relationship', 				['uses' => 'RelationshipController@relationship']);