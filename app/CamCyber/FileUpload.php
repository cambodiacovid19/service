<?php

namespace App\CamCyber;


use Illuminate\Http\Request;
use Image;

class FileUpload{
    
    public static function image(Request $request, $imageName ="", $folders = array(), $sizes = array()){
        if($request->hasFile($imageName)) {
            $image = $request->file($imageName);
            $extension = $image->getClientOriginalExtension(); 
            
         
            $directories = "";
            for($i=0; $i< sizeof($folders); $i++){
                $directories .= $folders[$i];
            }
            if(!file_exists(public_path($directories))){
                mkdir(public_path($directories) , 0777, true);
            }


            $uploadedImage = array();
            foreach($sizes as $size){
                $myImage = '/'.$size[1].'x'.$size[2].'.'.$extension;
                Image::make($image->getRealPath())->resize($size[1], $size[2])->save(public_path($directories).$myImage);
                $uploadedImage[$size[0]] = 'public/'.$directories.$myImage;
            }

            return json_encode($uploadedImage);


        }else{
            return "";
        }
    }

    public static function fileUpload(Request $request, $fileName ="", $folders = array()){
        if($request->hasFile($fileName)) {
            $file = $request->file($fileName);
            //dd($file);
            $extension = $file->getClientOriginalExtension(); 
            
         
            $directories = "";
            for($i=0; $i< sizeof($folders); $i++){
                $directories .= $folders[$i];
            }

            if(!file_exists(public_path($directories))){
                mkdir(public_path($directories) , 0777, true);
            }
            $fileName = time().'.'.$file->getClientOriginalExtension();
            $path = $file->move(public_path($directories), $fileName);
            return 'public/'.$directories.'/'.$fileName;
           


        }else{
            return "";
        }
    }

    public static function uploadImage(Request $request, $imageName ="", $folders = array(), $sizes = array()){
        if($request->hasFile($imageName)) {
            $image = $request->file($imageName);
            $extension = $image->getClientOriginalExtension(); 
            
         
            $directories = "";
            for($i=0; $i< sizeof($folders); $i++){
                $directories .= $folders[$i];
            }
            if(!file_exists(public_path($directories))){
                mkdir(public_path($directories) , 0777, true);
            }


            $uploadedImage = array();
            foreach($sizes as $size){
                $myImage = '/'.$size[1].'x'.$size[2].'.'.$extension;
                Image::make($image->getRealPath())->resize($size[1], $size[2])->save(public_path($directories).$myImage);
                $uploadedImage[$size[0]] = 'public/'.$directories.$myImage;
            }

            // return json_encode($uploadedImage);
            return 'public/'.$directories.$myImage;

        }else{
            return "";
        }
    }
   
    public static function resize(Request $request, $imageName ="", $folders = array(), $sizes = array()){
       
        if($request->hasFile($imageName)) {
            $image = $request->file($imageName);
            $extension = $image->getClientOriginalExtension(); 
            
         
            $directories = "";
            for($i=0; $i< sizeof($folders); $i++){
                $directories .= $folders[$i];
            }
            if(!file_exists(public_path($directories))){
                mkdir(public_path($directories) , 0777, true);
            }


            $uploadedImage = array();
            foreach($sizes as $size){
                $myImage = '/'.$size[1].'x'.$size[2].'.'.$extension;
                Image::make($image->getRealPath())->resize($size[1], $size[2],
                function($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path($directories).$myImage);
                $uploadedImage[$size[0]] = 'public/'.$directories.$myImage;
            }

            // return json_encode($uploadedImage);
            return 'public/'.$directories.$myImage;

        }else{
            return "";
        }
    }


    // Upload Image for with current project directory
    public static function attachFile($folder, $file){
        /**
         * Toget Extension cal $ext
        */
        if($file !="") {  
            $dataFile = explode(',', $file);
            $ini =substr($dataFile[0], 17);
            $ext = explode(';', $ini);
            $info = substr($file, 5, strpos($file, ';')-5);
            $extension = explode("/", $info);
            
            if(isset($extension[1])){
                $ext = strtolower($extension[1]);
                if($ext == "pdf" || $ext == "png" || $ext == "jpg" || $ext == "jpeg"){
                    /**
                     * Check if PDF Attachment
                     */
                    if($ext == 'pdf'){
                        if($file){
                            $folder = isset($folder)?$folder:"unknown";
                            
                            $fileName =  uniqid();
                            $image_parts = explode(";base64,", $file);
                            $image_base64 = base64_decode($image_parts[1]);

                            // Make folder
                            $mkdir_folder = public_path().'/'.$folder.'/';
                            if(!file_exists($mkdir_folder)){
                                File::makeDirectory($mkdir_folder, 0777,true);
                            }

                            $uri = public_path().'/lts/'.$folder.'/'.$fileName.'.'.$ext; 
                            file_put_contents($uri, $image_base64); 
                            $url_response = 'public/'.$folder.'/'.$fileName.'.'.$ext;
                            return $url_response;
                        }
                    }else{
                        /**
                         * Check if Image Attachment
                         */
                        if($file){
                            $folder = $folder = isset($folder)?$folder:"unknown";
                           
                            $image_parts = explode(";base64,", $file);
                            $image_type_aux = explode("image/", $image_parts[0]);
                            $extension = $image_type_aux[1];
                            $image_base64 = base64_decode($image_parts[1]);
                            $fileName = uniqid();
                            // Make folder
                            $mkdir_folder = public_path().'/'.$folder.'/';
                            if(!file_exists($mkdir_folder)){
                                File::makeDirectory($mkdir_folder, 0777,true);
                            }

                            $uri = public_path().'/'.$folder.'/'.$fileName.'.'.$ext; 
                            file_put_contents($uri, $image_base64);
                            $url_response = 'public/'.$folder.'/'.$fileName.'.'.$ext;
                            return $url_response;
                        } 
                    }
                }
            }
        }
        
    }

  
}
