<?php

namespace App\Api\V1\Controllers\CP\Hospital;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Hospital\Main;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

class Controller extends ApiController
{
    use Helpers;
    function listing() {
        $data = Main::select(
                'id', 
                'kh_name', 
                'en_name', 
                'phone',
                'kh_location',
                'en_location',
                'kh_address',
                'en_address',
                'lat',
                'lng',
                'created_at'
                )
        ->get();
        return response()->json($data, 200);
    }

    function view($id = 0){
        $data   = Main::select(
            'id', 
            'kh_name', 
            'en_name', 
            'phone',
            'kh_location',
            'en_location',
            'kh_address',
            'en_address',
            'lat',
            'lng',
            'created_at'
            )
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function create(Request $req)
    {
        $this->validate($req, [
            'kh_name'              => 'required|max:60',
            'en_name'              => 'required|max:60',
            'phone' =>  [
                'required', 
                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                Rule::unique('hospital', 'phone')
            ],
            'kh_location'              => 'max:250',
            'en_location'              => 'max:250',
            'kh_address'              => 'max:250',
            'en_address'              => 'max:250',
        ], [
            'kh_name.required'         => 'Plese enter province name.',
            'kh_name.max'              => 'Name in khmer must not be more then 60 chacters.',
            'en_name.required'         => 'Plese enter province name.',
            'en_name.max'              => 'Name in english must not be more then 60 chacters.',

            'phone.required'      =>   'Please enter your phone number.',
            'phone.regex'         =>   'Phone number is required.',
            'phone.unique'        =>   'Phone number already exists.',

            'kh_location.max'              => 'Location in khmer must not be more then 250 chacters.',
            'en_location.max'              => 'Location in english must not be more then 250 chacters.',

            'kh_address.max'              => 'Address in khmer must not be more then 250 chacters.',
            'en_address.max'              => 'Address in english must not be more then 250 chacters.',
        ]);

        $data                       = new Main();
        $data->kh_name              = $req->kh_name;
        $data->en_name              = $req->en_name;
        $data->phone                = $req->phone;
        $data->kh_location          = $req->kh_location;
        $data->en_location          = $req->en_location;
        $data->kh_address           = $req->kh_address;
        $data->en_address           = $req->en_address;
        $data->lat                  = $req->lat;
        $data->lng                  = $req->lng;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'kh_name'              => 'required|max:60',
            'en_name'              => 'required|max:60',
            'phone' =>  [
                'required', 
                'regex:/(^[0][0-9].{7}$)|(^[0][0-9].{8}$)/', 
                Rule::unique('hospital', 'phone')->ignore($id)
            ],
            'kh_location'              => 'max:250',
            'en_location'              => 'max:250',
            'kh_address'              => 'max:250',
            'en_address'              => 'max:250',
        ], [
            'kh_name.required'         => 'Plese enter province name.',
            'kh_name.max'              => 'Name in khmer must not be more then 60 chacters.',
            'en_name.required'         => 'Plese enter province name.',
            'en_name.max'              => 'Name in english must not be more then 60 chacters.',

            'phone.required'      =>   'Please enter your phone number.',
            'phone.regex'         =>   'Phone number is required.',
            'phone.unique'        =>   'Phone number already exists.',

            'kh_location.max'              => 'Location in khmer must not be more then 250 chacters.',
            'en_location.max'              => 'Location in english must not be more then 250 chacters.',

            'kh_address.max'              => 'Address in khmer must not be more then 250 chacters.',
            'en_address.max'              => 'Address in english must not be more then 250 chacters.',
        ]);

        $data   = Main::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->kh_name              = $req->kh_name;
            $data->en_name              = $req->en_name;
            $data->phone                = $req->phone;
            $data->kh_location          = $req->kh_location;
            $data->en_location          = $req->en_location;
            $data->kh_address           = $req->kh_address;
            $data->en_address           = $req->en_address;
            $data->lat                  = $req->lat;
            $data->lng                  = $req->lng;
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function delete($id = 0)
    {
        $data   = Main::find($id); 
        if($data){
            // Start to update
            $data->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

}
