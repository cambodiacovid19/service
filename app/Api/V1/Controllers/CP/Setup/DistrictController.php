<?php

namespace App\Api\V1\Controllers\CP\Setup;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Setup\District ;

use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

class DistrictController extends ApiController
{
    use Helpers;
    function listing(Request $req) {
        $data = District::select('id', 'province_id','code','kh_name', 'en_name', 'created_at')
        ->withCount([ 
            'case as n_of_members'
        ])
        ->with('province')
        ->get();
        return response()->json($data, 200);
    }

    function view($id = 0){
        $data   = District::select('*')
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function create(Request $req)
    {
        $this->validate($req, [
            'kh_name'              => 'required|max:20',
            'en_name'              => 'required|max:20',
        ], [
            'kh_name.required'         => 'Plese enter District name.',
            'kh_name.max'              => 'Province name must not be more then 20 chacters.',
            'en_name.required'         => 'Plese enter District name.',
            'en_name.max'              => 'District name must not be more then 20 chacters.',
        ]);

        $data                       = new District();
        $data->province_id          = $req->province_id;
        $data->code                 = $req->code;
        $data->kh_name              = $req->kh_name;
        $data->en_name              = $req->en_name;

        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'kh_name'              => 'required|max:20',
            'en_name'              => 'required|max:20',
        ], [
            'kh_name.required'         => 'Plese enter District name.',
            'kh_name.max'              => 'District name must not be more then 20 chacters.',
            'en_name.required'         => 'Plese enter District name.',
            'en_name.max'              => 'District name must not be more then 20 chacters.',
        ]);

        $data   = District::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->province_id          = $req->province_id;
            $data->code                 = $req->code;
            $data->kh_name              = $req->kh_name;
            $data->en_name              = $req->en_name;
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function delete($id = 0)
    {
        $data   = District::find($id); 
        if($data){
            // Start to update
            $data->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

}
