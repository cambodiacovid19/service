<?php

namespace App\Api\V1\Controllers\CP\VisitedCountry;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Visited\Main;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

class Controller extends ApiController
{
    use Helpers;
    function listing(Request $req) {
        $data = Main::select(
                'id', 
                'country_id',
                'case_id',
                'from',
                'to',
                'created_at'
                )
                ->with(['country','case'])
        ->get();
        return response()->json($data, 200);
    }

    function view($id = 0){
        $data   = Main::select(
            'id', 
            'country_id',
            'case_id',
            'from',
            'to',
            'created_at'
            )
            ->with(['country','case'])
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function create(Request $req)
    {
        $this->validate($req, [
            'country_id'               => 'required',
            'case_id'                   => 'required',
            'from'                      => 'required|date_format:Y-m-d',
            'to'                        => 'required|date_format:Y-m-d',
        ], [
            'country_id.required'              => 'Plese enter province.',
            'case_id.required'                     => 'Plese enter case.',

            'from.required'             => 'Plese enter confirmed date.',
            'from.date_format'          => 'Plese date format Y-m-d.',
            'to.required'               => 'Plese enter confirmed date.',
            'to.date_format'            => 'Plese date format Y-m-d.',
        ]);

        $data                       = new Main();
        $data->country_id          = $req->country_id;
        $data->case_id              = $req->case_id;
        $data->from                 = $req->from;
        $data->to                   = $req->to;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'country_id'               => 'required',
            'case_id'                   => 'required',
            'from'                      => 'required|date_format:Y-m-d',
            'to'                        => 'required|date_format:Y-m-d',
        ], [
            'country_id.required'              => 'Plese enter province.',
            'case_id.required'                     => 'Plese enter case.',

            'from.required'             => 'Plese enter confirmed date.',
            'from.date_format'          => 'Plese date format Y-m-d.',
            'to.required'               => 'Plese enter confirmed date.',
            'to.date_format'            => 'Plese date format Y-m-d.',
        ]);

        $data   = Main::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->country_id          = $req->country_id;
            $data->case_id              = $req->case_id;
            $data->from                 = $req->from;
            $data->to                   = $req->to;
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function delete($id = 0)
    {
        $data   = Main::find($id); 
        if($data){
            // Start to update
            $data->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

}
