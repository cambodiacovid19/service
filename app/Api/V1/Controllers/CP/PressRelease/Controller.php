<?php

namespace App\Api\V1\Controllers\CP\PressRelease;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\PressRelrease\Main;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

class Controller extends ApiController
{
    use Helpers;
    function listing(Request $req) {
        $data = Main::select(
                'id', 
                'kh_name', 
                'en_name', 
                'kh_description',
                'en_description',
                'kh_data',
                'en_data',
                'created_at'
                )
        ->get();
        return response()->json($data, 200);
    }

    function view($id = 0){
        $data   = Main::select(
                'id', 
                'kh_name', 
                'en_name', 
                'kh_description',
                'en_description',
                'kh_data',
                'en_data',
                'created_at'
            )
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function create(Request $req)
    {
        $this->validate($req, [
            'kh_name'              => 'required|max:60',
            'en_name'              => 'required|max:60',
           
            'kh_description'              => 'max:500',
            'en_description'              => 'max:500',
            'kh_data'                     => 'max:500',
            'en_data'                     => 'max:500',
        ], [
            'kh_name.required'         => 'Plese enter province name.',
            'kh_name.max'              => 'Name in khmer must not be more then 60 chacters.',
            'en_name.required'         => 'Plese enter province name.',
            'en_name.max'              => 'Name in english must not be more then 60 chacters.',

            'kh_description.max'              => 'Location in khmer must not be more then 500 chacters.',
            'en_description.max'              => 'Location in english must not be more then 500 chacters.',
            'kh_data.max'                     => 'Data in khmer must not be more then 500 chacters.',
            'en_data.max'                     => 'Data in english must not be more then 500 chacters.',
        ]);

        $data                       = new Main();
        $data->kh_name              = $req->kh_name;
        $data->en_name              = $req->en_name;
        $data->kh_description       = $req->kh_description;
        $data->en_description       = $req->en_description;
        $data->kh_data              = $req->kh_data;
        $data->en_data              = $req->en_data;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'kh_name'              => 'required|max:60',
            'en_name'              => 'required|max:60',
           
            'kh_description'              => 'max:500',
            'en_description'              => 'max:500',
            'kh_data'                     => 'max:500',
            'en_data'                     => 'max:500',
        ], [
            'kh_name.required'         => 'Plese enter province name.',
            'kh_name.max'              => 'Name in khmer must not be more then 60 chacters.',
            'en_name.required'         => 'Plese enter province name.',
            'en_name.max'              => 'Name in english must not be more then 60 chacters.',

            'kh_description.max'              => 'Location in khmer must not be more then 500 chacters.',
            'en_description.max'              => 'Location in english must not be more then 500 chacters.',
            'kh_data.max'                     => 'Data in khmer must not be more then 500 chacters.',
            'en_data.max'                     => 'Data in english must not be more then 500 chacters.',
        ]);

        $data   = Main::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->kh_name              = $req->kh_name;
            $data->en_name              = $req->en_name;
            $data->kh_description       = $req->kh_description;
            $data->en_description       = $req->en_description;
            $data->kh_data              = $req->kh_data;
            $data->en_data              = $req->en_data;
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function delete($id = 0)
    {
        $data   = Main::find($id); 
        if($data){
            // Start to update
            $data->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

}
