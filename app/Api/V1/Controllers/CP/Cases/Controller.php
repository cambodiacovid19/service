<?php

namespace App\Api\V1\Controllers\CP\Cases;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\CamCyber\FileUpload;
use App\Api\V1\Controllers\ApiController;
use App\Model\Cases\Main;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
// use App\Model\User as Model;

use App\Model\Event\Main as Event; 
use App\Model\Hospital\Main as Hospital; 
use App\Model\Setup\Province as Province;
use App\Model\Setup\Nationality as Nationality;
use App\Model\Setup\LinkedType as LinkedType;

class Controller extends ApiController
{
    use Helpers;
    function listing() {
        $data = Main::select(
                'id', 
                'province_id',
                'linked_case_id',
                'linked_type_id',
                'expose_id',
                'sex_id',
                'hospital_id',
                'event_id',
                'kh_name',
                'en_name',
                'number',
                'age',
                'confirmed_date',
                'discharged_date',
                'death_date',
                'created_at',
                'nationality_id'
                // 'is_discharge',
                // 'is_death'
        )
                ->with(['province','linkCase','linkType','sex', 'hospital', 'event', 'visited', 'expose', 'nationality']);

                $key = isset($_GET['key'])?$_GET['key']:"";
                if($key){
                    $data = $data->where('en_name', 'like', $key)->orWhere('kh_name', 'like', $key);
                }
    
                $from=isset($_GET['from'])?$_GET['from']:"";
                $to=isset($_GET['to'])?$_GET['to']:"";
                if(isValidDate($from)){
                    if(isValidDate($to)){
                        $from .=" 00:00:00";
                        $to .=" 23:59:59";
                        $data = $data->whereBetween('created_at', [$from, $to]);
                    }
                }
                $limit      =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
                $data= $data->orderBy('id', 'desc')->paginate($limit); 

        return response()->json($data, 200);
    }

    function view($id = 0){
        $data   = Main::select(
            'id', 
            'province_id',
            'linked_case_id',
            'linked_type_id',
            'expose_id',
            'sex_id',
            'hospital_id',
            'event_id',
            'kh_name',
            'en_name',
            'number',
          
            'age',
            'confirmed_date',
            'discharged_date',
            'death_date',
            'created_at',
            'nationality_id'
            
            )
            ->with(['province','linkCase','linkType','sex', 'hospital', 'event', 'visited', 'expose', 'nationality'])
        ->find($id); 

        if($data){
            return response()->json($data, 200);
        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function create(Request $req)
    {
        $this->validate($req, [
            'kh_name'                   => 'required|max:60',
            'en_name'                   => 'required|max:60',
            'nationality_id'            => 'required',
            'province_id'               => 'required',
            'expose_id'                 => 'required',
            'sex_id'                    => 'required',
            'number' =>  [
                'required',  
                Rule::unique('case', 'number')
            ],
            'age'                      => 'required',
        ], [
            'kh_name.required'         => 'Plese enter name.',
            'kh_name.max'              => 'Name in khmer must not be more then 60 chacters.',
            'en_name.required'         => 'Plese enter province name.',
            'en_name.max'              => 'Name in english must not be more then 60 chacters.',

            'province_id.required'       => 'Plese enter province.',
            'expose_id.required'         => 'Plese enter expose.',
            'linked_case_id.required'    => 'Plese enter link case.',
            'sex_id.required'            => 'Plese enter sex.',
            'hospital_id.required'       => 'Plese enter hospital.',
            'event_id.required'          => 'Plese enter event.',
            'number.required'            => 'Plese enter number.',
            'age.required'               => 'Plese enter age.',

            'confirmed_date.required'               => 'Plese enter confirmed date.',
            'confirmed_date.date_format'            => 'Plese date format Y-m-d.',
            'discharged_date.required'              => 'Plese enter confirmed date.',
            'discharged_date.date_format'           => 'Plese date format Y-m-d.',
            'death_date.required'                   => 'Plese enter confirmed date.',
            'death_date.date_format'                => 'Plese date format Y-m-d.',
        ]);

        $data                       = new Main();
        $data->nationality_id           = $req->nationality_id;
        $data->province_id              = $req->province_id;
        if($req->linked_case_id){
            $data->linked_case_id       = $req->linked_case_id;
        }
        $data->linked_type_id       = $req->linked_type_id;
        $data->sex_id               = $req->sex_id;
        $data->hospital_id          = $req->hospital_id;
        $data->event_id             = $req->event_id;
        $data->expose_id            = $req->expose_id;
        $data->kh_name              = $req->kh_name;
        $data->en_name              = $req->en_name;
        $data->number               = $req->number;
        $data->age                  = $req->age;
        $data->confirmed_date       = $req->confirmed_date;
        $data->discharged_date      = $req->discharged_date;
        $data->death_date           = $req->death_date;
        // $data->is_discharge         = $req->is_discharge = 1;
        // $data->is_death             = $req->is_death = 1;
        $data->save();

        return response()->json([
            'status' => 'success',
            'message' => 'បានបង្កើតដោយជោគជ័យ',
            'data' => $data,
        ], 200);
    }
    
    function update(Request $req, $id = 0)
    {
        $this->validate($req, [
            'kh_name'                   => 'required|max:60',
            'en_name'                   => 'required|max:60',
            'nationality_id'            => 'required',
            'province_id'               => 'required',
            'expose_id'                 => 'required',
            'sex_id'                    => 'required',
            'number' =>  [
                'required',  
                Rule::unique('case', 'number')->ignore($id)
            ],
            'age'                       => 'required',
        ], [
            'kh_name.required'         => 'Plese enter name.',
            'kh_name.max'              => 'Name in khmer must not be more then 60 chacters.',
            'en_name.required'         => 'Plese enter province name.',
            'en_name.max'              => 'Name in english must not be more then 60 chacters.',

            'nationality_id.required'         => 'Plese enter province.',
            'province_id.required'         => 'Plese enter province.',
            'expose_id.required'         => 'Plese enter province.',
            'linked_case_id.required'      => 'Plese enter link case.',
            'sex_id.required'           => 'Plese enter sex.',
            'hospital_id.required'           => 'Plese enter hospital.',
            'event_id.required'           => 'Plese enter event.',
            'number.required'           => 'Plese enter number.',
            'age.required'           => 'Plese enter age.',

            'confirmed_date.required'           => 'Plese enter confirmed date.',
            'confirmed_date.date_format'           => 'Plese date format Y-m-d.',
            'discharged_date.required'           => 'Plese enter confirmed date.',
            'discharged_date.date_format'           => 'Plese date format Y-m-d.',
            'death_date.required'           => 'Plese enter confirmed date.',
            'death_date.date_format'           => 'Plese date format Y-m-d.',
        ]);

        $data   = Main::find($id); 
        //========================================================>>>> Start to update
        if($data){
            // Start to update
            $data->nationality_id          = $req->nationality_id;
            $data->province_id          = $req->province_id;
            if($req->linked_case_id){
                $data->linked_case_id       = $req->linked_case_id;
            }
            $data->linked_type_id       = $req->linked_type_id;
            $data->sex_id               = $req->sex_id;
            $data->hospital_id          = $req->hospital_id;
            $data->event_id             = $req->event_id;
            $data->expose_id             = $req->expose_id;
    
            $data->kh_name              = $req->kh_name;
            $data->en_name              = $req->en_name;
            $data->number               = $req->number;
            $data->age                  = $req->age;
            $data->confirmed_date       = $req->confirmed_date;
            $data->discharged_date      = $req->discharged_date;
            $data->death_date           = $req->death_date;
            $data->is_discharge         = $req->is_discharge;
            $data->is_death             = $req->is_death;
            $data->save();
            return response()->json([
                'status' => 'success',
                'message' => 'បានធ្វើបច្ចុប្បន្នភាពដោយជោគជ័យ',
                'data' => $data
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }
    
    function delete($id = 0)
    {
        $data   = Main::find($id); 
        if($data){
            // Start to update
            $data->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'បានលុបដោយជោគជ័យ។',
            ], 200);

        }else{
            return response()->json([
                'status'  => 'fail',
                'message' => 'រកមិនឃើញកំណត់ត្រា។'
            ], 404);
        }
    }

    function verifyLinkedCases(Request $request){

        $number = Main::where('number',$request->number)
        ->select('id','kh_name','en_name','age')
        ->first();

        if($number){
            
            return response()->json([
                'member'        => $number,
                'status'        => 'success',
                'message'       => 'គណនីត្រូវបានដោយជោគជ័យ។', 
               
            ], 200); 

        }else{

            return response()->json([
                'message'       => 'ព័ត៌មានបញ្ជូនមិនត្រឹមត្រូវ។ សូមពិនិត្យម្តងទៀត។'
            ], 400); 
        }
    }

    function setupData(){
        return response()->json([
            'cases' => Main::select(
                'id', 
                'province_id',
                'linked_case_id',
                'linked_type_id',
                'expose_id',
                'sex_id',
                'hospital_id',
                'event_id',
                'number',
                'age',
                'nationality_id'
               
            )
            ->with(['province:id,kh_name','sex:id,kh_name', 'hospital', 'event:id,kh_name', 'nationality:id:kh_name'])
            ->get(), 
            'events' => Event::get(), 
            'hospitals' => Hospital::get(), 
            'provinces' => Province::get(), 
            'nationalities' => Nationality::get(), 
            'linked_types' => LinkedType::get()
        ], 200);
    }
}
