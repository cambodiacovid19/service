<?php

namespace App\Api\V1\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use JWTAuth;
use Carbon\Carbon;
use App\Api\V1\Controllers\ApiController;
use App\Model\User\Main as User;

use App\Model\Setup\Province;

use App\Model\User\Code;
use App\CamCyber\SMS;
use App\GCF\Account;
//========================== Use Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;

class RegisterController extends ApiController
{

    public function register(Request $request) {
        
        $user = User::where('phone',$request->phone)->orWhere('email', $request->email)->first();
            //return $user;
        if($user){
            //$user->delete();
        }
        
        $this->validate($request, [
            
            'name'      => 'required|max:60',
            'phone'     =>  [
                            'required',  
                             Rule::unique('user', 'phone')
                        ],
            'email'     =>   [
                            //'sometimes',  
                            'email', 
                            'max:100', 
                            Rule::unique('user', 'email')
                        ],
            'password'  => 'sometimes|required|min:6|max:60',
          

        ], [

                'uid.required'=>'Please enter uid.', 
                'name.required'=>'Please enter your name.', 

                'phone.unique'=>'This phone number has already been takend. Please try another one.', 
                'phone.regex'=>'Phone number is not valid. Please try another one.', 
                'phone.required'=>'phone number is required.', 

                'email.required'=>'E-mail is required.', 
                'email.unique'=>'The email has already been taken.', 
            ]);

                //====================================>> Create New user
                $user = new User();

                $user->type_id      = 2;
                
                $user->uid          = Account::generateProvineceUid();
                $user->name         = $request->input('name');
                $user->phone        = $request->input('phone');
                $user->email        = $request->input('email');
                $user->address      = $request->input('address');
                $user->is_active    = 0;
                $user->is_phone_verified    = 0;
                $user->is_email_verified    = 0;
                $user->password     = bcrypt($request->input('password'));
                $user->save();

               

                //$notification = $this->getSMSCode($request->input('phone'));
                
                return response()->json([
                    'status'        => 'success',
                    'message'       => 'Account has been successfully created.',
                    'data'          => $user,
                  
                    
            ], 200);
      
    }

    public function getSMSCode($phone) {
        
        $user = User::where(['phone'=>$phone,'deleted_at'=>null])->first(); 
       
        if($user){
            $code = new Code; 
            $code->user_id = $user->id; 
            $code->code = rand(100000, 999999);
            $code->type = 'VERIFY';
            $code->is_verified = 0; 
            $code->save(); 

            $notification = [
                'name'      => $user->name,
                'code'      => $code->code,
            ];
             $sms = SMS::sendSMS($user->phone, 'Please use this code :'.$code->code.' to verify your request. Thanks!');

            return $notification;
        }   

    }
   
    public function getEmailCode($email) {
        
        $user = User::where(['email'=>$email, 'deleted_at'=>null])->first(); 
       
        if($user){
            $code = new Code; 
            $code->user_id = $user->id; 
            $code->code = rand(100000, 999999);
            $code->type = 'VERIFY';
            $code->is_verified = 0; 
            $code->save(); 

            $notification = [
                'name'      => $user->name,
                'code'      => $code->code,
            ];
           
            Mail::to($user->email)->send(new Notification('Welcome New Member', $notification, 'emails.member.account.verify-email'));

            return $notification;
        }   

    }
    public function verifyAccount(Request $request) {
        
        
        $this->validate($request, [
            'username'  => 'required',
            'code'      => 'required|min:6',
        ]);
        
        $code = $request->post('code'); 

        $data = Code::where(['code'=>$code, 'type'=>'VERIFY'])->orderBy('id', 'DESC')->first(); 
        $totalMinutesDifferent = 0;
        if($data){
            //Check if expired
            $created_at = Carbon::parse($data->created_at);
            $now = Carbon::now(env('APP_TIMEZONE')); 
            $totalMinutesDifferent = $now->diffInMinutes($created_at);

            if($totalMinutesDifferent < 30){
                $user = User::findOrFail($data->user_id);
                if($user){
                    
                    //Updated Code
                    $code = Code::find($data->id); 
                    if($code->is_verified == 0){

                        $code->is_verified = 1; 
                        $code->verified_at = now(); 

                        $code->save(); 

                        $user->is_active = 1;

                        if(filter_var($request->post('username'), FILTER_VALIDATE_EMAIL)){
                           
                            if($user->email){
                                $user->is_email_verified = 1; 
                                $user->email_verified_at = now();
                            }
                        } else{
                            if($user->phone){    
                                $user->is_phone_verified = 1; 
                                $user->phone_verified_at = now();
                                //Send Notification
                                $sms = SMS::sendSMS($user->phone, 'Thanks for your registration. Your user ID is '.$user->uid.'. Enjoy Investing!');
                            }
                        }
                        $user->save();
                        //Crate token
                        $token = JWTAuth::fromUser($user);
                        return response()->json([
                            'status'=> 'success',
                            'token'=> $token 
                        ], 200);
                    }else{
                         return response()->json([
                            'status'=> 'error',
                            'message'=> 'code-already-verified' 
                        ], 200);
                    }
                    
                }else{
                     return response()->json([
                        'status'=> 'error',
                        'message'=> 'user-not-found' 
                    ], 200);
                }
            }else{
                return response()->json([
                    'status'=> 'error',
                    'message'=> 'code-expired'
                ], 200);
            }
                
        }else{
            return response()->json([
                'status'=> 'error',
                'message'=> 'code-not-valid' 
            ], 200);
        }
    }
    public function requestverifyCode(Request $request){
        $user = User::where('phone',$request->username)->orWhere('email', $request->username)->first();
        if($user){

            if(filter_var($request->post('username'), FILTER_VALIDATE_EMAIL)){
                $notification = $this->getEmailCode($request->input('username'));  
                return response()->json([
                    'status'        => 'success',
                    'message'       => 'code is been re-send', 
                    'notification'  => $notification,
                ], 200);     
              
            } else{
              $notification = $this->getSMSCode($request->input('username'));
               return response()->json([
                'status'        => 'success',
                'message'       => 'code is been re-send', 
                'notification'  => $notification,
            ], 200);   
            }
        }else{
            return response()->json([
                'status'=> 'error',
                'message'=> 'username is not valid' 
            ], 404);
        }
        
    }
    function provinces(Request $request){
       
        $data           = Province::select('id', 'name')
        ->get();
        return response()->json($data, 200);

    }

}
