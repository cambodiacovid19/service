<?php

namespace App\Api\V1\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use JWTAuth;
use TelegramBot;
use Carbon\Carbon;

use App\Api\V1\Controllers\ApiController;
use App\Model\User\Code;
use App\Model\User\Main as User;

//========================== Use Mail
use Illuminate\Support\Facades\Mail;
use App\Mail\Notification;

use App\CamCyber\SMS;

class ForgotPasswordController extends ApiController
{
    public function getResetPasswordCode(Request $request) {   
        $this->validate($request, [
             'username' => [
                            'required'
                        ],
        ]);
        
        $username = $request->username; 
        if(filter_var($username, FILTER_VALIDATE_EMAIL)){
            $user = User::where(['email'=>$username, 'type_id'=>2, 'deleted_at'=>null])->first(); 
        }else{
            $user = User::where(['phone'=>$username, 'type_id'=>2, 'deleted_at'=>null])->first(); 
        }
        if($user){
            
            $code = new Code; 
            $code->user_id = $user->id; 
            $code->code = rand(100000, 999999);
            $code->type = 'PASSWORD';
            $code->is_verified = 0; 
            $code->save(); 

            $notification = [
                'name'      => $user->name,
                'code'      => $code->code,
            ];

            if(filter_var($username, FILTER_VALIDATE_EMAIL)){
                Mail::to($user->email)->send(new Notification('Reset Password Code', $notification, 'emails.member.account.reset-password-code'));
            }else{
                $sms = SMS::sendSMS($user->phone, 'Please use this code :'.$code->code.' to verify your account. Thanks!');
            }

            return response()->json([
                'status'=> 'success',
                'message'=> 'code-has-been-sent',
            ], 200);
               
        }else{
            return response()->json([
                'status'=> 'error',
                'message'=> 'user-not-found' 
            ], 200);
        }

    }

    // public function getPhoneResetPasswordCode(Request $request) {   
    //     $this->validate($request, [
    //          'phone' => [
    //                         'required'
    //                     ],
    //     ]);
        
    //     $phone = $request->phone; 
    //     $user = User::where(['phone'=>$phone, 'type_id'=>2, 'deleted_at'=>null])->first(); 
       
    //     if($user){
            
    //         $code = new Code; 
    //         $code->user_id = $user->id; 
    //         $code->code = rand(100000, 999999);
    //         if($request->purpose == 'PASSWORD'){
    //             $code->type = 'PASSWORD';
    //         }else{
    //             $code->type = 'VERIFY';
    //         }
    //         $code->is_verified = 0; 
    //         $code->save(); 

    //         $notification = [
    //             'name'      => $user->name,
    //             'code'      => $code->code,
    //         ];
            
    //         $sms = SMS::sendSMS($user->phone, 'Please use this code :'.$code->code.' to verify your account. Thanks!');
    //         return response()->json([
    //             'status'=> 'success',
    //             'message'=> 'code-has-been-sent',
    //         ], 200);
               
    //     }else{
    //         return response()->json([
    //             'status'=> 'error',
    //             'message'=> 'user-not-found' 
    //         ], 200);
    //     }

    // }

    public function verifyResetPasswordCode(Request $request) {
        
        $this->validate($request, [
            'username' => 'required',
            'code' => 'required|min:6',
        ]);
        
        $code = $request->post('code'); 

        $data = Code::where(['code'=>$code, 'type'=>'PASSWORD'])->orderBy('id', 'DESC')->first(); 
        $totalMinutesDifferent = 0;
        if($data){
            //Check if expired
            $created_at = Carbon::parse($data->created_at);
            $now = Carbon::now(env('APP_TIMEZONE')); 
            $totalMinutesDifferent = $now->diffInMinutes($created_at);

            if($totalMinutesDifferent < 30){
                $user = User::findOrFail($data->user_id);
                if($user){
                    
                    //Updated Code
                    $code = Code::find($data->id); 
                    if($code->is_verified == 0){
                        $code->is_verified = 1; 
                        $code->verified_at = now(); 
                        $code->save(); 

                        $user->is_active = 1; 
                        $user->is_phone_verified = 1; 
                        
                        if(filter_var($request->post('username'), FILTER_VALIDATE_EMAIL)){
                           
                            if($user->email){
                                $user->is_email_verified = 1; 
                                $user->email_verified_at = now();
                            }
                        } else{
                            if($user->phone){    
                                $user->is_phone_verified = 1; 
                                $user->phone_verified_at = now();
                                //Send Notification
                                //$sms = SMS::sendSMS($user->phone, 'Thanks for your registration. Your user ID is '.$user->uid.'. Enjoy Investing!');
                            }
                        }
                        $user->save(); 
                        //Crate token
                        $token = JWTAuth::fromUser($user);
                        return response()->json([
                            'status'=> 'success',
                            'token'=> $token 
                        ], 200);
                    }else{
                         return response()->json([
                            'status'=> 'error',
                            'message'=> 'code-already-verified' 
                        ], 200);
                    }
                        


                }else{
                     return response()->json([
                        'status'=> 'error',
                        'message'=> 'user-not-found' 
                    ], 200);
                }
            }else{
                return response()->json([
                    'status'=> 'error',
                    'message'=> 'code-expired'
                ], 200);
            }
                
        }else{
            return response()->json([
                'status'=> 'error',
                'message'=> 'code-not-valid' 
            ], 200);
        } 
    }

    public function changePassword(Request $request) {
        
        $this->validate($request, [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6'
        ]);
        
        $user = JWTAuth::toUser($request->token);
        if($user){

            $user->password_last_updated_at = now();
            $user->password = bcrypt($request->input('password'));
            $user->save(); 

          
            return response()->json([
                'status'=> 'success',
                'message'=>'password has been succesfully changed.'
            ], 200);

        }else{
            return response()->json([
                'status'=> 'fail',
                'message'=>'invalid token'
            ], 401);
        }   

    }


    


}
