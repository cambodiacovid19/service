<?php

namespace App\Api\V1\Controllers\Client\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;
use App\Model\Cases\Main as Cases;
use App\Model\Setup\Nationality;
use App\Model\Setup\Province;
class Controller extends ApiController
{
    use Helpers;
    function getCaseAmount(){
        $cases = Cases::select('id'); 
        $total_cases = Cases::select('id');
        $total_confirmed_case = Cases::select('id');
        $total_discharged_case = Cases::select('id');
        $total_death_case = Cases::select('id');
        $total_local_expose_case = Cases::select('id');
        $total_imported_expose_case = Cases::select('id');

        $total_cases = $total_cases->count();
        $total_confirmed_case = $total_confirmed_case->whereNotNull('confirmed_date')->whereNull('discharged_date')->whereNull('death_date')->count();
        $total_discharged_case = $total_discharged_case->whereNotNull('discharged_date')->whereNull('death_date')->count();
        $total_death_case = $total_death_case->whereNotNull('death_date')->count();
        $total_local_expose_case = $total_local_expose_case->where('expose_id', 1)->count();
        $total_imported_expose_case = $total_imported_expose_case->where('expose_id', 2)->count();

        $nationals = Nationality::select('id', 'kh_name', 'en_name', 'color')->with('cases')->get();
        $national_data = [];
        $national_value = [];
        foreach($nationals as $row){
            $national_value[] =  ['value' => $row->cases->count(), 'itemStyle' => ['color' =>$row->color] ];
        }
        foreach($nationals as $row){
            $national_data[] =  $row->kh_name;
        }
        return response()->json(
            [
               'cases_sumuria' => [
                'total_cases'=>$total_cases,
                'total_confirmed_case' => $total_confirmed_case,
                'total_discharged_case' => $total_discharged_case,
                'total_death_case' => $total_death_case,
                'total_local_expose_case'=>$total_local_expose_case,
                'total_imported_expose_case'=>$total_imported_expose_case,
                'national_data' => $national_data,
                'national_value'   => $national_value
                ]
                
            ]
        , 200);
    }

    function dailyNewCase(){
        $date = Carbon::parse('2020-02-13 00:00:00');
        $now = Carbon::now();
        $diff = $date->diffInDays($now);
       $today = Carbon::parse('today');
       $cases          = []; 
       $labels          = []; 

       $dates = []; 
       
        for($i = 1 ; $i <= $diff; $i++){
            
            $start =  Carbon::parse('2020-02-13 00:00:00')->addDays($i)->format('Y-m-d 00:00:00');
            $end = Carbon::parse('2020-02-13 00:00:00')->addDays($i)->format('Y-m-d 23:59:59');
            
            $totalCases = Cases::select('id', 'confirmed_date')->whereNotNull('confirmed_date')
            ->whereBetween('confirmed_date', [$start, $end])
            ->count(); 

            $cases[] = $totalCases; 

           

            $labels[] = Carbon::parse('2020-02-13 00:00:00')->addDays($i)->format('d/m');  
            
        }
    
        return response()->json(
            [
                'cases'=>$cases,
                'labels' => $labels,
            ]
        , 200);
    }

    function dischargeCase(){
        $date = Carbon::parse('2020-02-13 11:00:00');
        $now = Carbon::now();
        $diff = $date->diffInDays($now);
       $today = Carbon::parse('today'); 
       $cases           = []; 
       $labels          = []; 
       $discharts       = []; 
       $dates = []; 
       
        for($i = 1 ; $i <= $diff; $i++){
            
            $start =  Carbon::parse('2020-02-13 00:00:00')->addDays($i)->format('Y-m-d 00:00:00');
            $end = Carbon::parse('2020-02-13 00:00:00')->addDays($i)->format('Y-m-d 23:59:59'); 
            
            // new case
            $totalCases = Cases::select('id', 'confirmed_date')->whereNotNull('confirmed_date')->whereNull('discharged_date')->whereNull('death_date')
            ->whereBetween('confirmed_date', [$start, $end])
            ->count(); 
            $cases[] = $totalCases; 
            // New Dischart
            $totalDischart = Cases::select('id', 'confirmed_date', 'discharged_date')->whereNotNull('discharged_date')->whereNull('death_date')
            ->whereBetween('discharged_date', [$start, $end])
            ->count(); 
            $discharts[] = $totalDischart; 

            $labels[] = Carbon::parse('2020-02-13 00:00:00')->addDays($i)->format('d/m'); 
            
        }
    
        return response()->json(
            [
                'cases'=>$cases,
                'discharts'=>$discharts,
                'labels' => $labels,
            ]
        , 200);
    }
  
    function getProvices(){
        $provinces = Province::select('id', 'kh_name', 'en_name', 'code')->with('cases')->get();
        $province_value = [];
        foreach($provinces as $row){
            $province_value[] =  ['name' => $row->en_name, 'code' => $row->code, 'value' =>  ($row->cases->count() == 0)?'':$row->cases->count() ];
        }
        return response()->json(
            [
                'provinces'=>$province_value
            ]
        , 200);
    }

}