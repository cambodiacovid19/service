<?php

namespace App\Api\V1\Controllers\Client\World;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use App\Model\Member\Main as Member;
use App\Model\Branch\Purchase as Branch;
use App\Model\Transaction\GCF as GCF;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;
use App\Model\Cases\Main as Cases;

class Controller extends ApiController
{
    use Helpers;
    function index(){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://raw.githubusercontent.com/amcharts/covid-charts/master/data/json/total_timeline.json');
        $result = curl_exec($ch);
        curl_close($ch);
        $total_timeline = json_decode($result);

        $ch1 = curl_init();
        curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch1, CURLOPT_URL, 'https://raw.githubusercontent.com/amcharts/covid-charts/master/data/json/world_timeline.json');
        $world = curl_exec($ch1);
        curl_close($ch1);
        
        $world_timeline = json_decode($world);
        return response()->json(['total_timeline' => $total_timeline, 'world_timeline', $world_timeline] ,200);
    }

  

}