<?php

namespace App\Api\V1\Controllers\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Api\V1\Controllers\ApiController;
use Dingo\Api\Routing\Helpers;
use JWTAuth;
use Carbon\Carbon;
use App\Model\Cases\Main as Data;
use App\Model\Event\Main as Event;
use App\Model\Setup\Nationality;
use App\Model\Setup\Province;
class RelationshipController extends ApiController
{
    use Helpers;
   

    function relationship(){

        $eventData = Event::select('id', 'kh_name as name')->with(['cases'])->get();
        //$data = Data::get(); 
        
        $events = []; 
        $cases = [];
        $links = []; 

        $relationss = [];

        $i = 0; 
        $j = 0; 

        // =================================>> Case that has Event
        foreach($eventData as $row){
            $events[] = ['name'=>$row->name]; 
            foreach($row->cases as $case){

                $status = 'កំពុងព្យាបាល';
                if(!is_null($case->discharged_date)){
                    $status = 'ជា('. Carbon::parse($case->discharged_date)->format('d/m').')'; 
                }elseif(!is_null($case->death_date)){
                    $status = 'ស្លាប(' . Carbon::parse($case->death_date)->format('d/m').')'; 
                }

                $cases[] = [
                    'case_id' => $case->id, 
                    'index' => $j, 
                    'category' => $i, 
                    'name' => $case->number, 
                    'value' => $case->id, 
                    'province' => $case->province->kh_name, 
                    'age' => $case->age, 
                    'sex' => $case->sex ? $case->sex->kh_name:"", 
                    'confirmed_date' => Carbon::parse($case->confirmed_date)->format('d/m'), 
                    'status' => $status, 
                    //'relations' => $case->relations
                ]; 

                if(isset($case->relations)){
                    $relations[] = [
                        'index' => $j, 
                        'childrend' => $case->relations
                    ];
                }

                $j++; 
            }

            $i++; 
        }

        // =================================>> Case that has NO Event
        $unEventCases = Data::where('event_id', null)->get(); 
        foreach($unEventCases as $case){

            $status = 'កំពុងព្យាបាល';
            if(!is_null($case->discharged_date)){
                $status = 'ជា('. Carbon::parse($case->discharged_date)->format('d/m').')'; 
            }elseif(!is_null($case->death_date)){
                $status = 'ស្លាប(' . Carbon::parse($case->death_date)->format('d/m').')'; 
            }

            $cases[] = [
                'case_id' => $case->id, 
                'index' => $j, 
                //'category' => $i, 
                'name' => $case->number, 
                'value' => $case->id, 
                'province' => $case->province->kh_name, 
                'age' => $case->age, 
                'sex' => $case->sex ? $case->sex->kh_name:"", 
                'confirmed_date' => Carbon::parse($case->confirmed_date)->format('d/m'), 
                'status' => $status
            ]; 

            if(isset($case->relations)){
                $relations[] = [
                    'index' => $j, 
                    'childrend' => $case->relations
                ];
            }

            $j++; 
        }

        foreach($cases as $case){
            foreach($relations as $relation){
                foreach($relation['childrend'] as $child){

                    if($case['case_id'] ==  $child->id){
                        $links[] = [
                            'source' => $relation['index'], 
                            'target' => $case['index'], 
                            
                        ];
                    }
                }

            }
        }

       

        return [
            'categories' => $events, 
            'nodes' => $cases, 
            'links' => $links, 
            //'relations' => $relations,
            
        ]; 
    }

   

    function checkIndex($cases, $caseId){
        $i = 0; 
        foreach($cases as $case){
            if($case['value'] == $caseId){
                return $i; 
            }   
            $i++; 
        }

        return ''; 
    }
  
   

}