<?php
namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Model\Order\Main as CustomerOrder;
use App\Model\Branch\Purchase as BranchPurchase;
use App\Model\Store\Purchase as StorePurchase;


class ApiController extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = []) {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);
        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors());
        }
    }

    public function generateOrderReceiptNumber(){
        $number = rand(10000000, 99999999); 

        $check = CustomerOrder::where('receipt_number', $number)->first(); 
        if($check){
            return $this->generateOrderReceiptNumber(); 
        }

        return $number; 
    }

    public function generateBranchPurchaseReceiptNumber(){
        $number = rand(10000000, 99999999); 

        $check = BranchPurchase::where('receipt_number', $number)->first(); 
        if($check){
            return $this->generateBranchPurchaseReceiptNumber(); 
        }

        return $number; 
    }

    public function generateStorePurchaseReceiptNumber(){
        $number = rand(10000000, 99999999); 

        $check = StorePurchase::where('receipt_number', $number)->first(); 
        if($check){
            return $this->generateStorePurchaseReceiptNumber(); 
        }

        return $number; 
    }

   
    
}