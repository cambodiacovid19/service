<?php

namespace App\Api\V1\Resources\News;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Api\V1\Resources\Package\PackageResource;

class NewsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $partern =NewResource::collection($this->collection);
        
        return  $partern;
       
    }
}
