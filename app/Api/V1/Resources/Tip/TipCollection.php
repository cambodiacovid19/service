<?php

namespace App\Api\V1\Resources\Tip;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Api\V1\Resources\Package\PackageResource;

class TipCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $partern =TipResource::collection($this->collection);
        
        return  $partern;
       
    }
}
