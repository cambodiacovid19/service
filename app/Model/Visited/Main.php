<?php

namespace App\Model\Visited;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'visited_country';

    public function country(){
        return $this->belongsTo('App\Model\Setup\Country', 'country_id');
    }

    public function case(){
        return $this->belongsTo('App\Model\Cases\Main', 'case_id');
    }
}
