<?php

namespace App\Model\PressRelrease;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'press_release';

    public function attachments(){
        return $this->hasOne('App\Model\PressRelease\Main', 'press_id');
    }
}
