<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{	
    protected $table = 'province';

    public function country(){
        return $this->belongsTo('App\Model\Setup\Country', 'country_id');
    }

    public function cases(){
        return $this->hasMany('App\Model\Cases\Main', 'province_id');
    }

    public function district(){
        return $this->hasOne('App\Model\Setup\District', 'district_id');
    }

}
