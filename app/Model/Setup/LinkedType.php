<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class LinkedType extends Model
{	
    protected $table = 'linked_type';

    public function cases(){
        return $this->hasMany('App\Model\Cases\Main', 'linked_type_id');
    }

}
