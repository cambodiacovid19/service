<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Sex extends Model
{	
    protected $table = 'sex';

    public function case(){
        return $this->hasOne('App\Model\Cases\Main', 'sex_id');
    }


}
