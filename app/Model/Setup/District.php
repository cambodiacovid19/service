<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{	
    protected $table = 'district';

    public function province(){
        return $this->belongsTo('App\Model\Setup\Province', 'province_id');
    }

    public function case(){
        return $this->hasOne('App\Model\Cases\Main', 'district_id');
    }

    public function commune(){
        return $this->hasOne('App\Model\Setup\Commune', 'commune_id');
    }
    
}
