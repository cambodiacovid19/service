<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{	
    protected $table = 'nationality';

    public function cases(){
        return $this->hasMany('App\Model\Cases\Main', 'nationality_id');
    }
}
