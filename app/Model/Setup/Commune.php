<?php

namespace App\Model\Setup;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{	
    protected $table = 'commune';

    public function case(){
        return $this->hasOne('App\Model\Cases\Main', 'commune_id');
    }

    public function district(){
        return $this->belongsTo('App\Model\Setup\District', 'district_id');
    }
   
}
