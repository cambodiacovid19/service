<?php

namespace App\Model\Hospital;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'hospital';

    public function case(){
        return $this->hasOne('App\Model\Case\Main', 'case_id');
    }

}
