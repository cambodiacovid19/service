<?php

namespace App\Model\Expose;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'expose';

    public function cases(){
        return $this->hasMany('App\Model\Cases\Main', 'expose_id');
    }

}
