<?php

namespace App\Model\PressAttachments;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'press_attachments';

    public function press(){
        return $this->belongsTo('App\Model\PressRelease\Main', 'press_id');
    }
}
