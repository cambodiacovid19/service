<?php

namespace App\Model\Event;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table = 'event';

    public function cases(){
        return $this->hasMany('App\Model\Cases\Main', 'event_id');
    }

   
}
