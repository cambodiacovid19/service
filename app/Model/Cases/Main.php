<?php

namespace App\Model\Cases;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Main extends Model
{	
	use SoftDeletes;
    protected $table ='case';

    public function nationality(){
        return $this->belongsTo('App\Model\Setup\Nationality', 'nationality_id');
    }

    public function province(){
        return $this->belongsTo('App\Model\Setup\Province', 'province_id');
    }

    public function district(){
        return $this->belongsTo('App\Model\Setup\District', 'district_id');
    }

    public function commune(){
        return $this->belongsTo('App\Model\Setup\Commune', 'commune_id');
    }

    public function linkCase(){
        return $this->belongsTo('App\Model\Cases\Main', 'linked_case_id', 'number');
    }

    public function relations(){
        return $this->hasMany('App\Model\Cases\Main', 'linked_case_id')->select('id');
    }

    public function linkType(){
        return $this->belongsTo('App\Model\Setup\LinkedType', 'linked_type_id');
    }

    public function sex(){
        return $this->belongsTo('App\Model\Setup\Sex', 'sex_id');
    }

    public function hospital(){
        return $this->belongsTo('App\Model\Hospital\Main', 'hospital_id');
    }

    public function event(){
        return $this->belongsTo('App\Model\Event\Main', 'event_id');
    }

    public function visited(){
        return $this->hasOne('App\Model\Visited\Main', 'case_id');
    }

    public function expose(){
        return $this->belongsTo('App\Model\Expose\Main', 'expose_id');
    }

}
