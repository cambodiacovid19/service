<?php

namespace App\Http\Middleware;

use Closure;
use App\GCF\Account; 
use JWTAuth;

class CamCyber
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $panel = "")
    {
       
       
        $user         = JWTAuth::parseToken()->authenticate();
        if($user){
            if( $panel == "cp" || $panel == "master"){
                if ($user->admin) {
                    return $next($request);
                }
            }else if($panel == "branch" ){
                if($user->customer && $user->customer->staff) {
                    return $next($request);
                }
            }else if($panel == "store"  ){
                
                if($user->customer && $user->customer->store) {
                    return $next($request);
                }
            }else if($panel == "customer"){
                if ($user->customer) {
                    return $next($request);
                }
            }
        }
       
         
        return response()->json([
            'status' => 'error',
            'message' => "Access denied", 
            
        ], 401);

       
    }
}
